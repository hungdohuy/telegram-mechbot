# encoding: utf-8

# Telegram API framework core imports
from telegram.ext import Dispatcher, CallbackContext, Filters, MessageHandler
from telegram import Update
from functools import wraps

import random
import re

import configurations.settings as settings

# Helper methods import
from utils.logger import get_logger
from utils.restriction import restricted

# Telegram API framework handlers imports
from telegram.ext import CommandHandler

# Init logger
logger = get_logger(__name__)


def init(dispatcher):
    """Provide handlers initialization."""
    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("raffle", raffle))


@restricted
def raffle(update, context):
    """Pick a random user for raffle"""
    r1 = random.randint(0, len(settings.MEM_ALLOWED) - 1)
    reply = "Lucky member is [{}](tg://user?id={}) 🎁 🎁 🎁".format(
        settings.MEM_ALLOWED_NAME[r1], settings.MEM_ALLOWED[r1]
    )
    update.effective_message.reply_text(reply, parse_mode="Markdown")
