# coding: utf-8
import signal
import sys
import os

from threading import Thread

from telegram.ext import Updater, Dispatcher, CommandHandler, Filters
from importlib import import_module

import utils.logger as logging
import configurations.settings as settings


def load_handlers(dispatcher: Dispatcher):
    """Load handlers from files in a 'bot' directory."""
    base_path = os.path.join(os.path.dirname(__file__), "bot")
    files = os.listdir(base_path)

    for file_name in files:
        if (
            # Load only the python file for the bot command/feature anb not the feature in the beta mod defined in the configuration file
            file_name.endswith(".py")
            and file_name.split(".")[0] not in settings.BETA_FUNCS
        ):
            handler_module, _ = os.path.splitext(file_name)

            module = import_module(f".{handler_module}", "bot")
            module.init(dispatcher)

    dispatcher.add_handler(
        CommandHandler(
            "restart", restart, filters=Filters.user(user_id=settings.ADMINS)
        )
    )


def graceful_exit(*args, **kwargs):
    """Provide a graceful exit from a webhook server."""
    if updater is not None:
        updater.bot.delete_webhook()
    sys.exit(1)


def stop_and_restart():
    """Gracefully stop the Updater and replace the current process with a new one"""
    updater.stop()
    os.execl(sys.executable, sys.executable, *sys.argv)


def restart(update, context):
    """Calling restart bot by command"""
    logger.info("Receiving restart from user: {}".format(update.message.from_user.id))
    update.message.reply_text("Bot is restarting...")
    Thread(target=stop_and_restart).start()


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


if __name__ == "__main__":
    global updater
    logging.init_logger(f"{os.path.dirname(os.path.realpath(__file__))}/logs/{settings.NAME}.log")
    logger = logging.get_logger(__name__)
    updater = Updater(settings.TOKEN, use_context=True)

    load_handlers(updater.dispatcher)

    # Check the bot start with webhook mode or not
    if settings.WEBHOOK:
        signal.signal(signal.SIGINT, graceful_exit)
        updater.start_webhook(**settings.WEBHOOK_OPTIONS)
        updater.bot.set_webhook(url=settings.WEBHOOK_URL)
    else:
        # Start the Bot
        updater.start_polling()
        # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()
