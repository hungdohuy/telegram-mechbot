# encoding: utf-8

# Telegram API framework core imports
from telegram.ext import Dispatcher, CallbackContext, Filters, MessageHandler
from telegram import Update
from functools import wraps

import re

import configurations.settings as settings

# Helper methods import
from utils.logger import get_logger
from utils.restriction import restricted

# Init logger
logger = get_logger(__name__)


def init(dispatcher):
    """Provide handlers initialization."""
    # on noncommand i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(~Filters.command, parsing))


@restricted
def parsing(update, context):
    """Parsing the chat from users"""
    p = re.compile(r"", re.IGNORECASE)
    m = p.match(update.message.text)
    if m:
        logger.info("Match for the regex {}.".format(m.group()))
    else:
        logger.info("No match")
