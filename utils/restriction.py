import configurations.settings as settings
from functools import wraps

# Helper methods import
from utils.logger import get_logger

# Init logger
logger = get_logger(__name__)


def restricted(func):
    """Decorator function which check the user authorization for interating with Bot command"""

    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        if settings.USER_RESTRICT and user_id not in settings.MEM_ALLOWED:
            logger.warning("Unauthorized access denied for {}.".format(user_id))
            return
        return func(update, context, *args, **kwargs)

    return wrapped
