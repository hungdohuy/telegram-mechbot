# encoding: utf-8

# Telegram API framework core imports
from telegram.ext import Dispatcher, CallbackContext, Filters, MessageHandler
from telegram import Update
from functools import wraps

import configurations.settings as settings

# Helper methods import
from utils.logger import get_logger
from utils.restriction import restricted

# Telegram API framework handlers imports
from telegram.ext import CommandHandler

# Init logger
logger = get_logger(__name__)


def init(dispatcher):
    """Provide handlers initialization."""
    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("update", update))


@restricted
def update(update, context):
    """Update code"""
    reply = "Updating..."
    update.message.reply_text(reply, parse_mode="Markdown")
