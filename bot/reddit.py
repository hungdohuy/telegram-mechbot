# encoding: utf-8

import praw
import pprint
import re
import configurations.settings as settings
from functools import wraps

# Telegram API framework core imports
from telegram.ext import Dispatcher, CallbackContext
from telegram import Update
from datetime import datetime, timedelta

# Helper methods import
from utils.logger import get_logger

# Telegram API framework handlers imports
from telegram.ext import CommandHandler

from utils.restriction import restricted

# Init logger
logger = get_logger(__name__)
all_camp_jobs = dict()


def init(dispatcher):
    """Provide handlers initialization.

    Arguments:
        dispatcher {Dispatcher} -- [description]
    """ """"""
    dispatcher.add_handler(CommandHandler("search", mechmarket_search, pass_args=True))
    dispatcher.add_handler(CommandHandler("s", mechmarket_search, pass_args=True))
    dispatcher.add_handler(
        CommandHandler(
            "camp",
            mechmarket_camp,
            pass_args=True,
            pass_job_queue=True,
            pass_chat_data=True,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            "c",
            mechmarket_camp,
            pass_args=True,
            pass_job_queue=True,
            pass_chat_data=True,
        )
    )
    dispatcher.add_handler(
        CommandHandler("uncamp", mechmarket_uncamp, pass_args=True, pass_chat_data=True)
    )
    dispatcher.add_handler(
        CommandHandler("uc", mechmarket_uncamp, pass_args=True, pass_chat_data=True)
    )
    dispatcher.add_handler(CommandHandler("listcamp", mechmarket_listcamp))
    dispatcher.add_handler(CommandHandler("lc", mechmarket_listcamp))


def reddit_init():
    """Initialize the reddit instance with anonymous mode for search

    Returns:
        reddit instance -- [description]
    """ """"""
    try:
        reddit = praw.Reddit(
            client_id=settings.REDDIT_CLIENT_ID,
            client_secret=settings.REDDIT_CLIENT_SECRET,
            user_agent=settings.REDDIT_SUBRED,
        )
        logger.debug("Initialize subreddit instance")
        return reddit.subreddit(settings.REDDIT_SUBRED)
    except Exception:
        logger.error("Error while initialize reddit instance", exc_info=True)


def search(arg):
    """Perform a submission search in a subreddit

    Keyword Arguments:
        query {str} -- string to search (default: {""})
        limit {int} -- number of post you want to limit (default: {settings.REDDIT_SEARCH_LIMIT})
        sort {str} -- sort type for the search `new`, `top`, `relevance`, `hot`, `comments` (default: {settings.REDDIT_SEARCH_SORT})
        time_filter {str} -- `all`, `day`, `hour`, `month`, `week`, `year` (default: `all`) (default: {settings.REDDIT_SEARCH_TIME})
        flair {str} -- flair to search in reddit (default: {settings.REDDIT_SEARCH_FLAIR})

    Returns:
        list -- list of submission
    """
    sort = settings.REDDIT_SEARCH_SORT
    time_filter = settings.REDDIT_SEARCH_TIME
    flair = ""
    flairs = dict({"s": "Selling", "b": "Buying", "t": "Trading"})
    limits = []
    query = []

    for e in arg:
        r = re.match(r"^top(\d+)$", e)
        f = re.match(r"^f:(s|b|t)$", e)
        if r:
            limits.append(r.group(1))
        elif f:
            flair = flairs[f.group(1)]
        else:
            query.append(e)

    # print(query,limits)
    limit = (
        int(max(limits))
        if limits and 0 < int(max(limits)) <= 50
        else settings.REDDIT_SEARCH_LIMIT
    )

    subreddit = reddit_init()
    if not query:
        str_query = "flair:Selling"
    elif not flair:
        str_query = " ".join(query)
    else:
        str_query = " ".join(query) + " AND flair:" + flair
    logger.info(
        'Search details "{}" limit {} on r/{}'.format(
            str_query, limit, subreddit.display_name,
        )
    )
    submissions = subreddit.search(
        query=str_query, sort=sort, limit=limit, time_filter=time_filter,
    )

    return list(submissions)


@restricted
def mechmarket_search(update: Update, context: CallbackContext):
    """Send a message when the command /search <keyword> is issued.

    Arguments:
        update {Update} -- updater
        context {CallbackContext} -- context
    """ """"""

    # pprint.pprint(vars(update))
    if update.message:
        logger.info(
            "User {}:{} perform a search".format(
                update.message.from_user.id, update.message.from_user.username
            )
        )
    elif update.edited_message:
        logger.info(
            "User {}:{} perform a search".format(
                update.edited_message.from_user.id,
                update.edited_message.from_user.username,
            )
        )

    posts = search(context.args)

    # pprint.pprint(vars(posts))
    # reply="Results for: \"{}\"\n".format(' '.join(context.args))
    reply = prepare_reply(posts)
    if len(reply) == 0:
        reply += empty_search()
    update.effective_message.reply_text(
        reply, parse_mode="Markdown", disable_web_page_preview=True
    )


def prepare_reply(posts):
    """Reformat the search before reply

    Arguments:
        posts {list} -- list of the post

    Returns:
        str -- string of formated and conver location to flag ready to send reply
    """
    reply = ""
    for p in posts:
        reply += "{3}\[{4}]\{0} [{1}]({2})\n".format(
            (p.title.strip()[:45] + "...")
            if len(p.title.strip()) > 48
            else p.title.strip(),
            str(
                datetime.now().replace(microsecond=0)
                - datetime.fromtimestamp(p.created_utc)
            ),
            p.url,
            flag(p.title[1:3]),
            p.link_flair_text[:1],
        )
    return reply


def get_submision_ids(search_result):
    """Getting submission id from the search

    Arguments:
        search_result {list} -- list returned after search

    Returns:
        list -- list of submission id
    """
    ids = []
    if len(search_result) > 0:
        for s in search_result:
            ids.append(s.id)
    return ids


def flag(code):
    """Conver country code to flag icon

    Arguments:
        code {str} -- 2 characters of country code

    Returns:
        char -- unicode for flag
    """
    OFFSET = 127462 - ord("A")
    code = code.upper()
    return chr(ord(code[0]) + OFFSET) + chr(ord(code[1]) + OFFSET)


def empty_search():
    """Default empty return search

    Returns:
        str -- Return message
    """
    return "Nothing can be found!!!"


@restricted
def mechmarket_camp(update: Update, context: CallbackContext):
    """Camping command on subreddit for items, triggered by /camp or /c

    Arguments:
        update {Update} -- [description]
        context {CallbackContext} -- [description]
    """
    logger.info(
        "User {} called camp check with args {}".format(
            update.message.from_user.id, context.args
        )
    )

    if (update.effective_user.id in all_camp_jobs) and (
        "job" in all_camp_jobs[update.effective_user.id]
    ):
        logger.info(
            "Stop exiting camping job for: {}".format(
                " ".join(all_camp_jobs[update.effective_user.id]["job"].context[0])
            )
        )
        old_job = all_camp_jobs[update.effective_user.id]["job"]
        old_job.schedule_removal()

    update.effective_message.reply_text(
        "Start camping for: {}".format(" ".join(context.args))
    )
    new_job = context.job_queue.run_repeating(
        camp_check,
        interval=settings.REDDIT_CAMP_INTERVAL,
        first=0,
        context=[context.args, update.effective_user.id, update],
    )
    user_jobs = {"job": new_job, "last_search": []}
    all_camp_jobs.update({update.effective_user.id: user_jobs})


def camp_check(context: CallbackContext):
    """Camping job being called by the command /camp or /c

    Arguments:
        context {CallbackContext} -- [description]
    """
    (query, user_id, update) = context.job.context
    logger.info("Job starts for user {} searching for: {}".format(user_id, query))
    posts = search(query)
    new_search = get_submision_ids(posts)
    last_search = all_camp_jobs[user_id]["last_search"]
    logger.debug("New search: {}".format(new_search))
    logger.debug("Last search: {}".format(last_search))

    post_diff = list(set(new_search) - set(last_search))
    new_posts = []
    if len(post_diff) > 0:
        logger.info("New post detected for {}:{}".format(query, post_diff))
        for p in posts:
            if p.id in post_diff:
                new_posts.append(p)
        reply = prepare_reply(new_posts)
        update.effective_message.reply_text(
            text=reply, parse_mode="Markdown", disable_web_page_preview=True,
        )
        all_camp_jobs[user_id]["last_search"] = new_search
    else:
        logger.info("No new post for: {}".format(query))


@restricted
def mechmarket_uncamp(update: Update, context: CallbackContext):
    """Remove camping items

    Arguments:
        update {Update} -- [description]
        context {CallbackContext} -- [description]
    """
    if (update.effective_user.id in all_camp_jobs) and (
        "job" in all_camp_jobs[update.effective_user.id]
    ):
        current_camp = all_camp_jobs[update.effective_user.id]["job"].context[0]
        logger.info("Detect current job camping for: {}".format(" ".join(current_camp)))
        job = all_camp_jobs[update.effective_user.id]["job"]
        logger.info("Remove job schedule and delete job")
        job.schedule_removal()
        del all_camp_jobs[update.effective_user.id]["job"]
        update.message.reply_text("Stop camping for: {}".format(" ".join(current_camp)))
    else:
        update.message.reply_text("You have no camp!, use /camp or /c to set one")


@restricted
def mechmarket_listcamp(update: Update, context: CallbackContext):
    """Listing all current camp by user

    Arguments:
        update {Update} -- [description]
        context {CallbackContext} -- [description]
    """
    list_camp = []
    for user in all_camp_jobs.keys():
        if "job" in all_camp_jobs[user]:
            list_camp.append(
                all_camp_jobs[user]["job"].context[2].effective_user.full_name
                + ": "
                + " ".join(all_camp_jobs[user]["job"].context[0])
            )
    if len(list_camp) > 0:
        update.message.reply_text(
            "{} camps on-going:\n{}".format(len(list_camp), "\n".join(list_camp))
        )
    else:
        update.message.reply_text("No one is looking for anything, out of 💵")
