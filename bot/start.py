# encoding: utf-8
import sys
import os

# Telegram API framework core imports
from telegram.ext import Dispatcher, CallbackContext, Filters
from telegram import Update

# Helper methods import
from utils.logger import get_logger
from utils.restriction import restricted

# Telegram API framework handlers imports
from telegram.ext import CommandHandler

from threading import Thread

# Init logger
logger = get_logger(__name__)


def init(dispatcher: Dispatcher):
    """Provide handlers initialization."""
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))


@restricted
def start(update: Update, context: CallbackContext):
    """Process a /start command."""
    update.effective_message.reply_text(
        text="I'm a mechkey bot, have fun with mechanical keyboard and telegram"
    )


def help(update: Update, context: CallbackContext):
    """Send a message when the command /help is issued."""
    commands = """
        /help - List all the command usage
        /raffle - Pick a lucky member for GA
        /search or /s <keyword> - Search stuffs on r/mm with tag 'Selling'
        /camp or /c <keyword> - Camping stuffs on r/mm every 5mins(only 1 camp per user)
        /uncamp or /uc - Stop current camp
        /listcamp or /lc - List all on-going camp from users
    """
    update.effective_message.reply_text(
        "Here are the list of command: {}".format(commands)
    )
