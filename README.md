# Telegram bot in Python for mechanical keyboard enthusiast
This project is following the project skeleton of [Seed project](https://github.com/alesanmed/python-telegram-bot-seed), thanks to [Ale Sánchez](https://github.com/alesanmed)
- [Telegram bot in Python for mechanical keyboard enthusiast](#telegram-bot-in-python-for-mechanical-keyboard-enthusiast)
  - [Introduction](#introduction)
  - [Features list](#features-list)
  - [Project structure](#project-structure)
  - [Create virtualenv & Install dependencies](#create-virtualenv--install-dependencies)
  - [Edit config file](#edit-config-file)
  - [Begin implementing your commands](#begin-implementing-your-commands)
  - [Run your bot](#run-your-bot)

## Introduction

This bot was created with the initial need from a small group that share the same hobby about mechanical keyboard, there will be some function from the bot which is give-away for lucky member, search the selling post on subreddit r/mechmarket and camping the stuff as soon as it appears in the market

This project has been developed using Python 3.7 (but is compatible with, at least, python 3.5) and assumes that you use have virtualenv and pre-commit installed.

The project uses as the underlying technology [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).

The project also allows you to deploy the bot as a webhook or as a polling bot.

## Features list

- [x] Give away/raffle
- [x] Search reddit `r/mechmarket`
- [x] Camping item on reddit `r/mechmarket`
- [ ] Persistent search/camping data
- [ ] Staticstic of the searched items
- [ ] Price check for items
- [ ] Reminder for groupbuy start/close


## Project structure
```bash
.
├── assets
├── bot
│   ├── start.py
│   └── ...
├── configurations
│   └── settings.py
├── connectors
│   └── __init__.py
├── logs
├── utils
│   ├── __init__.py
│   └── logger.py
│   └── restriction.py
├── LICENSE
├── main.py
├── requirements.txt
├── .pre-commit-config.yaml
├── .gitlab-ci.yml
└── README.md
```

Let's see how is this project structured:
- assets: Here you will place all your assets files such as icons, images, audio and video files, etc.
- bot: Here you will place all your bot [command](#begin-implementing-your-commands) files.
- configurations: As it name says, this folder will contains all of your bot configuration files.
- connectors: This folder is intented to hold any connector your bot will need (such as databases, APIs, ect.)
- logs: Pretty self-explanatory, just the logs folder.
- utils: Here you will place any of your bot utils files. For example, files containing sets of helper functions, restriction users
- LICENSE: Just the license file you may want to change it for your project.
- main.py: The project entry point.
- requirements.txt: Dependencies used by project.
- .pre-commit-config.yaml: pre-commit configuration
- .gitlab-ci.yml: CI/CD for the repository
- README.md: This file.

## Create virtualenv & Install dependencies

You just have to run
```bash
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r requirements.txt
pre-commit install
```
from the project directory

## Edit config file

The default config file placed at `configurations/settings.py.sample` has the following default content, configure it with your information and put them in `settings.py`
```python
TOKEN = ""
NAME = ""
WEBHOOK = False
## The following configuration is only needed if you setted WEBHOOK to True ##
WEBHOOK_OPTIONS = {
    "listen": "0.0.0.0",  # IP
    "port": 443,
    "url_path": TOKEN,  # This is recommended for avoiding random people
    # making fake updates to your bot
}
WEBHOOK_URL = f'https://example.com/{WEBHOOK_OPTIONS["url_path"]}'
LOGLEVEL = "DEBUG"
MEM_ALLOWED = []
ADMINS = []
BETA_FUNCS = [
    'beta_function'
]
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""
REDDIT_SUBRED = ""
REDDIT_SEARCH_SORT = ""
REDDIT_SEARCH_LIMIT =
REDDIT_SEARCH_TIME = ""
REDDIT_SEARCH_FLAIR = ""
REDDIT_CAMP_INTERVAL =
```

- **TOKEN**: Your bot's token. You have to edit this and place the token that BotFather gave to your bot.
- **NAME**: Your bot's name. Optional. This is used for identifying your bot in places like the log file.
- **WEBHOOK**: If set to True, you will have to edit the other configuration options, for making a full-working webhook bot. If you just want to deploy a standard bot that automatically polls to the Telegram [getUpdates](https://core.telegram.org/bots/api#getupdates) method, leave it to False.
- **IP**: The IP your bot will be listening on. Usually you'll leave it a 0.0.0.0 as your bot will be listening on localhost.
- **PORT**: The port your bot will be listening on. If you don't have a reverse proxy you'll leave it to 443 (or any other supported port like 80, 88 or 8443). If you have a reverse proxy, you'll change it to the port on which you want your bot to listen on. After that, you have to configure your reverse proxy to redirect all incoming traffic from https://example.com/TOKEN to IP:PORT.
- **URL_PATH**: The path you set in the setWebhook call. It is recommended to leave it equal to your bot's token, but you can change it if you want.
- **WEBHOOK_URL**: Your full webhook URL. Normally here you'll just edit the domain name. But you can edit the full URL if you want.
- **LOGLEVEL**: Logging level follow the [python's logging level](https://docs.python.org/3/library/logging.html#levels)
- **MEM_ALLOWED**: List if user IDs which are allow to interact with this bot, in case you want to made it private for a specific group
- **ADMINS**: List of user IDs with the admin role for this bot
- **BETA_FUNCS**: List of all the fucntion which is in development and still in beta mode
- The following parameter is related to the interation with reddit, please refer to the usage of [praw](https://praw.readthedocs.io/en/latest/index.html)
  - **REDDIT_CLIENT_ID**: client app ID
  - **REDDIT_CLIENT_SECRET**: client secret
  - **REDDIT_USER_AGENT**: user agent must follow the rule of Reddit
  - **REDDIT_SUBRED**: subreddit which you would like to search for submission(in this case `mechmarket`)
  - **REDDIT_SEARCH_SORT**: sort type for the search `new`, `top`, `relevance`, `hot`, `comments`
  - **REDDIT_SEARCH_LIM**: limit the number of submission in the return
  - **REDDIT_SEARCH_TIME**: `all`, `day`, `hour`, `month`, `week`, `year` (default: `all`).
  - **REDDIT_SEARCH_FLAIR**: flair to search in Reddit
  - **REDDIT_CAMP_INTERVAL**: interval of camping option to search in reddit in second

## Begin implementing your commands
Implementing new commands is easy, but you might want to take a look at the [python-telegram-bot documentation](https://python-telegram-bot.org/).

Now that you are quite an expert at python-telegram bot, let's see how to implement new commands.

You just have to create a file that will hold all your command functionality. Let's take a closer look at this process.

Imagine you want to implement a `start` command. First, create your command file at the `bot/` directory. It is not required, but we recommend you to name the file as your command. As the command would be `start` is a good idea to name the file `start` too, i.e. `bot/start.py`. This file will hold all of your command code. The only requirement is that your command file has to implement a **at least** an `init` function. So the second thing you have to do is... yes, implement your command logic.

All main functions from command files receive the same argument, the *dipatcher*. This way, you can implement any type of command. I'll give you an example for the *start* command:
```python
def init(dispatcher: Dispatcher):
    """Provide handlers initialization."""
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))


@restricted
def start(update: Update, context: CallbackContext):
    """Process a /start command."""
    update.effective_message.reply_text(text="I'm a mechkey bot, have fun with mechanical keyboard and telegram")


def help(update: Update, context: CallbackContext):
    """Send a message when the command /help is issued."""
    commands = """
        /help - List all the command usage
        /raffle - Pick a lucky member for GA
        /search or /s <keyword> - Search stuffs on r/mm with tag 'Selling'
        /camp or /c <keyword> - Camping stuffs on r/mm every 5mins(only 1 camp per user)
        /uncamp or /uc - Stop current camp
        /listcamp or /lc - List all on-going camp from users
    """
    update.effective_message.reply_text(
        "Here are the list of command: {}".format(commands)
    )
```

## Run your bot

You just have to run
```bash
source .venv/bin/activate
python main.py
```

And you're done. Next time you launch your bot, it'll respond to the `/start` command.

![Demo](assets/demo.gif)